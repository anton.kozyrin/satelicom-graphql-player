## 1.1.0 (2020-02-04)

- Update GraphiQL CDN latest
- Update React version to 16

## 1.0.0 (2019-05-14)

- Initial build.
