Satelicom Control System GraphQL Player v. 1.1
==============================================

This is a tool to test the SCS GraphQL endpoint using the Oauth2 Token.
Please follow the following instructions in order to configure the test 
environment.

1. Open the `ScsUtils.js` file in the root directory.
2. At line 3 replace the string `PUT-HERE-YOUR-TOKEN` with the provided token
3. Open the file index.html in your browser (Chrome or Firefox)

You can use the browser inspector to see how to submit the request to the SCS endpoint.
